import { Router } from 'express';

import env from './env';

const router = Router();

/**
 * GET /api
 */
router.get('/', (req, res) => {
  res.json({ name: env.name, version: env.version });
});

export default router;
