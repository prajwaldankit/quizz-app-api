import dotenv from 'dotenv';

/**
 * Initialize environment variables.
 */
dotenv.config();

// The env used byt the api
export default {
  nodeEnv: process.env.NODE_ENV,
  name: process.env.NAME,
  version: process.env.VERSION,
  port: process.env.PORT,
  db: process.env.DB,
  dbAuth: process.env.DB_AUTH,
  jwtSecret: process.env.JWT_SECRET,
  backendHost: process.env.BACKEND_HOST,
  frontendHost: process.env.FRONTEND_HOST,
  mailTransporter: process.env.MAIL_TRANSPORTER ? process.env.MAIL_TRANSPORTER : 'ethereal',
  mailUser: process.env.MAIL_USER,
  mailPass: process.env.MAIL_PASS,
};
