import mongoose from 'mongoose';

import env from './env';

const dbOptions = {
  authSource: env.dbAuth,
  useNewUrlParser: true,
  useCreateIndex: true, // to solve this issue: (node:6928) DeprecationWarning: collection.ensureIndex is deprecated. Use createIndexes instead.
  useUnifiedTopology: true,
};

mongoose
  .connect(env.db, dbOptions)
  .then((result) => {
    console.log('Mongoose is connected');
  })
  .catch((err) => {
    console.log('Mongoose connection unsuccessful');
  });
