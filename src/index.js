import env from './env';

import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

const corsOptions = {
  origin: '*',
};

const app = express();

app.use(
  express.urlencoded({
    extended: true,
  }),
);
app.use(express.json());
app.use(cors(corsOptions));
app.use(morgan('combined'));

app.listen(env.port, (err, succ) => {
  if (!err) {
    console.log(`server is up and running at ${env.port}`);
  } else {
    console.log('server could not be started');
  }
});

export default app;
